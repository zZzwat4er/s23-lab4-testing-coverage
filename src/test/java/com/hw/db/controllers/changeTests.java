package com.hw.db.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;

public class changeTests {
    private final JdbcTemplate mockJdbc = mock(JdbcTemplate.class);;
    private final String nick = "a";

    @BeforeEach
    @DisplayName("User Change MC/DC tests")
    void init() {
        new UserDAO(mockJdbc);
    }

    @Test
    @DisplayName("user change test 1")
    void changeTest1() {
        UserDAO.Change(new User(nick, null, null, null));
        verify(mockJdbc, never()).update(any(String.class));
    }

    @Test
    @DisplayName("user change test 2")
    void changeTest2() {
        UserDAO.Change(new User(nick, "a", null, null));
        verify(mockJdbc).update(eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                any(Object.class), any(Object.class));
    }

    @Test
    @DisplayName("user change test 3")
    void changeTest3() {
        UserDAO.Change(new User(nick, null, "a", null));
        verify(mockJdbc).update(eq("UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
                any(Object.class), any(Object.class));
    }

    @Test
    @DisplayName("user change test 4")
    void changeTest4() {
        UserDAO.Change(new User(nick, null, null, "a"));
        verify(mockJdbc).update(eq("UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;"),
                any(Object.class), any(Object.class));
    }

    @Test
    @DisplayName("user change test 5")
    void changeTest5() {
        UserDAO.Change(new User(nick, "a", "a", null));
        verify(mockJdbc).update(eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                any(Object.class), any(Object.class), any(Object.class));
    }

    @Test
    @DisplayName("user change test 6")
    void changeTest6() {
        UserDAO.Change(new User(nick, "a", null, "a"));
        verify(mockJdbc).update(eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                any(Object.class), any(Object.class), any(Object.class));
    }

    @Test
    @DisplayName("user change test 7")
    void changeTest7() {
        UserDAO.Change(new User(nick, null, "a", "a"));
        verify(mockJdbc).update(eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                any(Object.class), any(Object.class), any(Object.class));
    }

    @Test
    @DisplayName("user change test 8")
    void changeTest8() {
        UserDAO.Change(new User(nick, "a", "a", "a"));
        verify(mockJdbc).update(eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                any(Object.class), any(Object.class), any(Object.class), any(Object.class));
    }
}
