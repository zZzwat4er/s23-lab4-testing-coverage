package com.hw.db.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;

public class userListTest {

    private final JdbcTemplate mockJdbc = mock(JdbcTemplate.class);;

    @BeforeEach
    @DisplayName("User List full branch tests")
    void init() {
        new ForumDAO(mockJdbc);
    }

    @Test
    @DisplayName("no condition succeeded")
    void standardBranchTest() {
        ForumDAO.UserList("slug", null, null, null);
        verify(mockJdbc).query(Mockito.eq(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("limit condition succeeded")
    void limitBranchTest() {
        ForumDAO.UserList("slug", 10, null, null);
        verify(mockJdbc).query(Mockito.eq(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("desc condition succeeded")
    void descBranchTest() {
        ForumDAO.UserList("slug", null, null, true);
        verify(mockJdbc).query(Mockito.eq(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("since condition succeeded 1")
    void sinceBranchTest1() {
        ForumDAO.UserList("slug", null, "asdf", null);
        verify(mockJdbc).query(Mockito.eq(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("since condition succeeded 2")
    void sinceBranchTest2() {
        ForumDAO.UserList("slug", null, "asdf", true);
        verify(mockJdbc).query(Mockito.eq(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}
