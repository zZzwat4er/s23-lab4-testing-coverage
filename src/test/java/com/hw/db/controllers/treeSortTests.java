package com.hw.db.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;

public class treeSortTests {
    private final JdbcTemplate mockJdbc = mock(JdbcTemplate.class);

    @BeforeEach
    @DisplayName("tests set 4")
    void init() {
        new ThreadDAO(mockJdbc);
    }

    @Test
    @DisplayName("test 1")
    void test1() {
        ThreadDAO.treeSort(1, 1, 1, true);
        verify(mockJdbc).query(eq(
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                any(PostDAO.PostMapper.class), any(Object.class), any(Object.class), any(Object.class));

    }

    @Test
    @DisplayName("test 2")
    void test2() {
        ThreadDAO.treeSort(1, 1, 1, false);
        verify(mockJdbc).query(eq(
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
                any(PostDAO.PostMapper.class), any(Object.class), any(Object.class), any(Object.class));

    }
}
