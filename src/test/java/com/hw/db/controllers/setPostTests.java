package com.hw.db.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.eq;

import java.sql.Timestamp;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;

public class setPostTests {
    private final JdbcTemplate mockJdbc = mock(JdbcTemplate.class);

    /*
     * If we draw a graph of set post function then we get the following graph
     * 1 if author changed
     * | \
     * | 7
     * | /
     * 2 if message changed
     * | \
     * | \
     * | 8 if there were changes before
     * | / \
     * | 9 10
     * | / /
     * |<-----
     * 3 if creation date changed
     * | \
     * | \
     * | 11 if there were changes before
     * | / \
     * | 12 13
     * | / /
     * |<-----
     * 4 if we apply changes
     * | \
     * 5 6
     * 
     * Thus the least amount of paths to wisit all nodes is 4 and paths are:
     * 1 2 3 4 5 - change nothing
     * 1 7 2 8 9 3 11 12 4 6 - change everything
     * 1 2 8 10 3 4 6 - change only message
     * 1 2 3 11 13 6 - change only creation date
     */

    private Post otherPost = new Post(
            "a",
            new Timestamp(124124),
            "forum",
            "a",
            1,
            1,
            true);;

    private final Integer id = 1;

    @BeforeEach
    @DisplayName("User Change Basic path tests")
    void init() {
        new PostDAO(mockJdbc);
        Mockito.when(mockJdbc.queryForObject(any(String.class), any(PostDAO.PostMapper.class), any(Integer.class)))
                .thenReturn(otherPost);
    }

    @Test
    @DisplayName("Post without changes")
    void noChange() {
        PostDAO.setPost(id, new Post(
                null,
                null,
                "forum",
                null,
                1,
                1,
                true));
        verify(mockJdbc, never()).update(any(String.class));
    }

    @Test
    @DisplayName("Post without changes")
    void changeAll() {
        PostDAO.setPost(id, new Post(
                "author",
                new Timestamp(0),
                "forum",
                "massage",
                1,
                1,
                true));
        verify(mockJdbc).update(eq(
                "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                any(Object.class), any(Object.class), any(Object.class), any(Object.class));

    }

    @Test
    @DisplayName("Post without changes")
    void changeMassage() {
        PostDAO.setPost(id, new Post(
                null,
                null,
                "forum",
                "massage",
                1,
                1,
                true));
        verify(mockJdbc).update(eq(
                "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                any(Object.class), any(Object.class));

    }

    @Test
    @DisplayName("Post without changes")
    void changeCreated() {
        PostDAO.setPost(id, new Post(
                null,
                new Timestamp(0),
                "forum",
                null,
                1,
                1,
                true));
        verify(mockJdbc).update(eq(
                "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                any(Object.class), any(Object.class));

    }

}
