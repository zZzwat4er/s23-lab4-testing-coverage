package com.hw.db.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;

public class forumDAOTests {

    private final JdbcTemplate mockJdbc = mock(JdbcTemplate.class);;

    @BeforeEach
    @DisplayName("forum creation test")
    void init() {
        new ForumDAO(mockJdbc);
    }

    @Test
    @DisplayName("User gets list of threads test #1")
    void ThreadListTest1() {
        // There is some strange voodoo magic going on
        // And using static variables is very bad technique
        ForumDAO.ThreadList("slug", null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created;"),
                Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #2")
    void ThreadListTest2() {
        ForumDAO.ThreadList("slug", null, "12.02.2019", null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created;"),
                Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #3")
    void ThreadListTest3() {
        ForumDAO.ThreadList("slug", null, "12.02.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created desc;"),
                Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #4")
    void ThreadListTest4() {
        ForumDAO.ThreadList("slug", 10, "12.02.2019", true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created<=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created desc LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #5")
    void ThreadListTest5() {
        ForumDAO.ThreadList("slug", 10, "12.02.2019", null);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT AND created>=(?)::TIMESTAMP WITH TIME ZONE ORDER BY created LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

    @Test
    @DisplayName("User gets list of threads test #6")
    void ThreadListTest6() {
        // There is some strange voodoo magic going on
        // And using static variables is very bad technique
        ForumDAO.ThreadList("slug", 10, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM threads WHERE forum = (?)::CITEXT ORDER BY created desc LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(ThreadDAO.ThreadMapper.class));
    }

}
